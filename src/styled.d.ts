// import original module declarations
import 'styled-components';

// and extend them!
declare module 'styled-components' {
  export interface DefaultTheme {
    colors: {
      dark: string;
      dark2: string;
      dark3: string;
      black: string;
      orange: string;
      mint: string;
      lightGrey: string;
      grey: string;
      grey2: string;
      white: string;
      text: {
        white: string;
        grey: string;
        grey2: string;
        dark: string;
        dark2: string;
        dark3: string;
      },
    },
    fontSizes: {
      xxs: string;
      xs: string;
      sm: string;
      md: string;
      lg: string; 
      xl: string; 
      xl2: string;
      xl3: string;
      xl4: string;
      xl5: string;
      xl6: string;
      xl7: string;
    },
    fontWeights: {
      hairline: number;
      thin: number;
      light: number;
      normal: number;
      medium: number;
      semibold: number;
      bold: number;
    },
    borderRadius: {
      sm: string;
      md: string;
      circle: string;
    } 
    breakpoints: {
      sm: string;
      md: string;
      lg: string;
      xl: string;
      xl2: string;
    }
  }
}