import React, { useEffect } from 'react';
import { Provider } from 'react-redux';
import { HashRouter as Router, Switch, Route, useLocation } from 'react-router-dom';

import store from '../redux/store';
import Theme from '../styles/Theme';
import GlobalStyles from '../styles/GlobalStyles';
import User from '../pages/User';
import Home from '../pages/Home';
import Users from '../pages/Users';

const NotFound = (): JSX.Element => {
  return <h1>Page not found</h1>;
};

const ScrollToTop = (): null => {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }, [pathname]);

  return null;
};

const App = (): JSX.Element => {
  return (
    <Provider store={store}>
      <Theme>
        <Router>
          <GlobalStyles />
          <ScrollToTop />
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/users' component={Users} />
            <Route exact path='/users/:login/:section' component={User} />
            <Route component={NotFound} />
          </Switch>
        </Router>
      </Theme>
    </Provider>
  );
};

export default App;
