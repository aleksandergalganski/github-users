import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useLocation } from 'react-router-dom';

import { Loader } from '../Loader';
import { RootState } from '../../redux/reducers';
import { searchUsers } from '../../redux/actions/usersActions';
import {
  Container,
  BottomSection,
  ResultsCount,
  Alert,
  AlertText
} from './Freelancers.elements';
import { Pagination } from './Pagination';
import { LoaderContainer } from '../../styles/GlobalStyles';
import { Users } from '../Users';

const Freelancers = (): JSX.Element => {
  const location = useLocation();
  const dispatch = useDispatch();
  const users = useSelector((state: RootState) => state.users.data);
  const isLoading = useSelector((state: RootState) => state.users.isLoading);
  const totalCount = useSelector((state: RootState) => state.users.totalCount);

  useEffect(() => {
    if (location.search) {
      const params = new URLSearchParams(location.search);
      let page = 1;
      let q = '';

      if (params.has('page')) {
        page = Number(params.get('page'));
      }

      if (params.has('q')) {
        q = params.get('q') as string;
      }

      dispatch(searchUsers({ page, q }));
    }
  }, [dispatch, location]);

  return (
    <Container>
      {isLoading && (
        <LoaderContainer verticalPadding='2'>
          <Loader />
        </LoaderContainer>
      )}
      {(users.length > 0) ? (
        <Users users={users} />
      ) : (
        totalCount === 0 && !isLoading ? (
          <Alert>
            <AlertText>Can not find user! Type something else.</AlertText>
          </Alert>
        ) : (
          <Alert>
            <AlertText>Type something to search users!</AlertText>
          </Alert>
        )
      )}
      {users.length > 0 && (
        <BottomSection>
          <ResultsCount>Showing {users.length}/{totalCount}</ResultsCount>
          <Pagination />
        </BottomSection>
      )}
    </Container>
  );
};

export default Freelancers;
