import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useLocation, useHistory } from 'react-router-dom';

import { RootState } from '../../../redux/reducers';
import arrowLeft from '../../../assets/arrow-left.svg';
import arrowRight from '../../../assets/arrow-right.svg';
import {
  Pagination as PaginationContainer,
  PaginationButton,
  ArrowIcon
} from './Pagination.elements';

const Pagination = (): JSX.Element => {
  const [nextPage, setNextPage] = useState<number>(2);
  const [prevPage, setPrevPage] = useState<number>(0);
  const [canGoNext, setCanGoNext] = useState<boolean>(false);
  const location = useLocation();
  const history = useHistory();
  const totalCount = useSelector((state: RootState) => state.users.totalCount);

  useEffect(() => {
    if (location.search) {
      const params = new URLSearchParams(location.search);
      let page = 1;

      if (params.has('page')) {
        page = Number(params.get('page'));
        setNextPage(page + 1);

        if (page > 1) {
          setPrevPage(page - 1);
        } else {
          setPrevPage(0);
        }
      }

      if (totalCount) {
        const usersPerPage = 12;
        setCanGoNext(usersPerPage * page < totalCount);
      }
    }
  }, [location, totalCount]);

  const push = (num: number): void => {
    const params = new URLSearchParams(location.search);
    params.delete('page');
    params.append('page', String(num));

    history.push(`/users?${params.toString()}`);
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  return (
    <PaginationContainer>
      <PaginationButton
        variant='white'
        disabled={!prevPage}
        onClick={() => push(prevPage)}
      >
        <ArrowIcon src={arrowLeft} alt='Left arrow' />
      </PaginationButton>
      <PaginationButton
        variant='orange'
        disabled={!canGoNext}
        onClick={() => push(nextPage)}
      >
        <ArrowIcon src={arrowRight} alt='Right arrow' />
      </PaginationButton>
    </PaginationContainer>
  );
};

export default Pagination;
