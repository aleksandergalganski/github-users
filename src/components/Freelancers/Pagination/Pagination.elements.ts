import styled , { css } from 'styled-components';

export const Pagination = styled.div`
  display: flex;
  align-items: center;
`;

type PaginationButtonProps = {
  variant: 'white' | 'orange';
};

export const PaginationButton = styled.button<PaginationButtonProps>`
  width: 60px;
  height: 60px;
  padding: 22px 26px;
  border-radius: 10px;
  border: none;
  cursor: pointer;
  display: inline-block;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: background-color 0.3s ease-in;

  ${(props) =>
    props.variant === 'white' &&
    css`
      border: solid 1px rgba(223, 223, 223, 0.7);
      background-color: ${({ theme }) => theme.colors.white};
    `};

  ${(props) =>
    props.variant === 'orange' &&
    css`
      background-color: ${({ theme }) => theme.colors.orange};
    `};

  &:first-child {
    margin-right: 10px;
  }

  &:disabled {
    cursor: default;
  }
`;

export const ArrowIcon = styled.img`
  width: 8px;
  height: 16px;
`;
