import styled from 'styled-components';

export const Container = styled.main`
  min-height: 50vh;
  max-width: 80%;
  margin: 0 auto;
`;

export const BottomSection = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-bottom: 60px;
`;

export const ResultsCount = styled.div`
  font-size: ${({ theme }) => theme.fontSizes.md};
  line-height: 1.76;
  color: ${({ theme }) => theme.colors.text.grey2};
`;

export const Alert = styled.div`
  background-color: ${({ theme }) => theme.colors.orange};
  border-radius: 10px;
  margin: 2rem 0;
`;

export const AlertText = styled.p`
  color: ${({ theme }) => theme.colors.white};
  font-size: ${({ theme }) => theme.fontSizes.xl};
  font-weight: ${({ theme }) => theme.fontWeights.semibold};
  padding: 2rem;
  text-align: center;

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    font-size: ${({ theme }) => theme.fontSizes.md};
  }
`;
