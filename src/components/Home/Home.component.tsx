import React from 'react';

import { HomeHeader } from './HomeHeader';
import { Testimonials } from './Testimonials';
import { Container } from './Home.elements';
import { AboutUs } from './AboutUs';

const Home = (): JSX.Element => {
  return (
    <>
      <HomeHeader />
      <Container>
        <AboutUs />
        <Testimonials />
      </Container>
    </>
  );
};

export default Home;
