import React from 'react';

import { TextBoxContainer, TextBoxLeft, Title, Header, TextBoxRight, Description} from './HorizontalTextBox.elements';

type TextBoxProps = {
  title: string;
  header: string;
  description: string;
};

const HorizontalTextBox = ({ title, header, description }: TextBoxProps): JSX.Element => {
  return (
    <TextBoxContainer>
      <TextBoxLeft>
        <Title>{title}</Title>
        <Header>{header}</Header>
      </TextBoxLeft>
      <TextBoxRight>
        <Description>{description}</Description>
      </TextBoxRight>
    </TextBoxContainer>
  );
};

export default HorizontalTextBox;
