import styled from 'styled-components';

export const TextBoxContainer = styled.article``;

export const Title = styled.h5`
  opacity: 0.7;
  font-size: ${({ theme }) => theme.fontSizes.md};
  font-weight: ${({ theme }) => theme.fontWeights.normal};
  color: ${({ theme }) => theme.colors.text.dark};
  padding-bottom: 5rem;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    padding-bottom: 1rem;
  }
`;

export const Header = styled.h4`
  font-size: 48px;
  font-weight: ${({ theme }) => theme.fontWeights.bold};
  line-height: 1.21;
  letter-spacing: -1px;
  color: ${({ theme }) => theme.colors.text.dark};
  padding-bottom: 1.5rem;

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    font-size: ${({ theme }) => theme.fontSizes.xl4};
  }
`;

export const Description = styled.p`
  font-size: ${({ theme }) => theme.fontSizes.md};
  line-height: 1.76;
  color: ${({ theme }) => theme.colors.text.grey2};
`;
