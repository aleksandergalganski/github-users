import React from 'react';

import {
  TextBoxContainer,
  Title,
  Header,
  Description
} from './VerticalTextBox.elements';

type TextBoxProps = {
  title: string;
  header: string;
  description: string;
};

const VerticalTextBox = ({ title, header, description }: TextBoxProps): JSX.Element => {
  return (
    <TextBoxContainer>
      <Title>{title}</Title>
      <Header>{header}</Header>
      <Description>{description}</Description>
    </TextBoxContainer>
  );
};

export default VerticalTextBox;