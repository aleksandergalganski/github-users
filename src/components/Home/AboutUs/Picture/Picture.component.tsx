import React from 'react';

import { PictureContainer, BackgroundImage, LogoImage } from './Picture.elements';

type PictureProps = {
  backgroundPath: string;
  backgroundAlt?: string;
  logoPath: string;
  logoAlt?: string;
  alignment: 'top-left' | 'bottom-left' | 'bottom-right'
  gridColumn: string;
  gridRow: string;
};

const Picture = ({ 
  backgroundPath, 
  backgroundAlt = '', 
  logoPath, 
  logoAlt = '', 
  alignment, 
  gridColumn, 
  gridRow 
}: PictureProps): JSX.Element => {
  return (
    <PictureContainer gridColumn={gridColumn} gridRow={gridRow}>
      <BackgroundImage
        src={require(`../../../../assets/landing/${backgroundPath}`)}
        alt={backgroundAlt}
      />
      <LogoImage
        alignment={alignment}
        src={require(`../../../../assets/landing/${logoPath}`)}
        alt={logoAlt}
      />
    </PictureContainer>
  );
};

export default Picture;
