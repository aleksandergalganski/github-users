import styled, { css } from 'styled-components';

type PictureContainerProps = {
  gridColumn: string;
  gridRow: string;
};

export const PictureContainer = styled.div<PictureContainerProps>`
  position: relative;
  grid-row: ${({ gridRow }) => gridRow};
  grid-column: ${({ gridColumn }) => gridColumn};

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    grid-column: auto;
    grid-row: auto;
  }
`;

export const BackgroundImage = styled.img`
  width: 100%;
  height: 100%;
`;

type LogoImageProps = {
  alignment: 'top-left' | 'bottom-left' | 'bottom-right';
};

export const LogoImage = styled.img<LogoImageProps>`
  position: absolute;
  width: 113px;
  height: 48px;

  ${(props) =>
    props.alignment === 'top-left' &&
    css`
      top: 10px;
      left: 10px;
    `};

  ${(props) =>
    props.alignment === 'bottom-left' &&
    css`
      left: 10px;
      bottom: 10px;
    `};

  ${(props) =>
    props.alignment === 'bottom-right' &&
    css`
      right: 10px;
      bottom: 10px;
    `};
`;
