import styled from 'styled-components';

export const TopSection = styled.section`
  display: flex;
  justify-content: space-between;
  padding-bottom: 8rem;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    flex-direction: column;
    padding-bottom: 2rem;
  }
`;

export const TopLeft = styled.div`
  flex-basis: 40%;
  padding-right: 2rem;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    flex-basis: auto;
    padding-right: 0;
  }
`;

export const TopRight = styled.div`
  flex-basis: 50%;
  display: flex;
  justify-content: flex-end;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    flex-basis: auto;
  }
`;

export const TopRightInner = styled.div`
  position: relative;
`;

export const TopRightImage = styled.img`
  width: 100%;
  max-width: 500px;
  height: auto;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    max-width: 100%;
  }
`;

export const CenterSection = styled.section`
  display: flex;
  justify-content: space-between;
  padding-bottom: 8rem;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    flex-direction: column-reverse;
    padding-bottom: 2rem;
  }
`;

export const CenterLeft = styled.div`
  flex-basis: 50%;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    flex-basis: auto;
    padding-right: 0;
  }
`;

export const CenterRight = styled.div`
  flex-basis: 40%;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    flex-basis: auto;
    padding-bottom: 1rem;
  }
`;

export const BottomSection = styled.section`
  padding-bottom: 4rem;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    padding-bottom: 1rem;
  }
`;

export const Stats = styled.div`
  background-color: ${({ theme }) => theme.colors.orange};
  position: absolute;
  height: auto;
  box-shadow: 0 54px 100px -10px rgba(255, 121, 77, 0.3);
  z-index: 1000;
  left: -40%;
  top: 5%;
  width: 60%;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    position: static;
    width: 100%;
    padding-bottom: 1rem;
    margin-top: 1rem;
  }
`;

export const StatsInner = styled.div`
  padding: 3rem;
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    flex-direction: row;
  }
`;

export const StatItem = styled.div`
  flex-basis: 50%;

  &:first-of-type {
    padding-bottom: 4rem;
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    flex-basis: 50%;
    text-align: center;

    &:first-of-type {
      padding-bottom: 0;
    }
  }
`;

export const Count = styled.h6`
  font-size: 48px;
  font-weight: ${({ theme }) => theme.fontWeights.bold};
  line-height: 1.21;
  letter-spacing: -1px;
  color: ${({ theme }) => theme.colors.white};
  padding-bottom: 1rem;

  @media (max-width: ${({ theme }) => theme.breakpoints.sm}) {
    font-size: ${({ theme }) => theme.fontSizes.xl4};
  }
`;

export const Description = styled.p`
  opacity: 0.7;
  font-size: ${({ theme }) => theme.fontSizes.xl};
  line-height: 1.52;
  color: ${({ theme }) => theme.colors.white};

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    padding: 0 1rem;
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.sm}) {
    font-size: ${({ theme }) => theme.fontSizes.md};
  }
`;

export const Pictures = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-template-rows: repeat(5, 1fr);
  grid-gap: 2rem;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    grid-gap: 1rem;
    grid-template-columns: 1fr;
    grid-template-rows: repeat(3, auto);
  }
`;
