import React from 'react';

import { VerticalTextBox } from './VerticalTextBox';
import { HorizontalTextBox } from './HorizontalTextBox';
import aboutUsImage from '../../../assets/landing/about-us-image.png';
import { Picture } from './Picture';
import { 
  TopSection,
  TopLeft,
  TopRight,
  TopRightInner,
  Stats,
  StatsInner,
  StatItem,
  Count,
  Description,
  TopRightImage,
  CenterSection,
  CenterLeft,
  Pictures,
  CenterRight,
  BottomSection
} from './AboutUs.elements';

const description =
  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas aliquet orci, sit sagittis sed vel dictum lacus purus.  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas aliquet orci, sit sagittis sed vel dictum lacus purus.';

const AboutUs = (): JSX.Element => {
  return (
    <>
      <TopSection>
        <TopLeft>
          <VerticalTextBox
            title='O1. About Us'
            header='All you need to know about us.'
            description={description}
          />
        </TopLeft>
        <TopRight>
          <TopRightInner>
            <Stats>
              <StatsInner>
                <StatItem>
                  <Count>1200+</Count>
                  <Description>Our users are all over the world</Description>
                </StatItem>
                <StatItem>
                  <Count>600+</Count>
                  <Description>Companies corporate with us</Description>
                </StatItem>
              </StatsInner>
            </Stats>
            <TopRightImage src={aboutUsImage} alt='' />
          </TopRightInner>
        </TopRight>
      </TopSection>
      <CenterSection>
        <CenterLeft>
          <Pictures>
            <Picture
              backgroundPath='image-airbnb.png'
              logoPath='logo-airbnb.png'
              alignment='bottom-right'
              gridColumn='2 / span 1'
              gridRow='1 / span 3'
            />
            <Picture
              backgroundPath='image-google.png'
              logoPath='logo-google.png'
              alignment='bottom-left'
              gridColumn='2 / span 1'
              gridRow='4 / span 2'
            />
            <Picture
              backgroundPath='image-amazon.png'
              logoPath='logo-amazon.png'
              alignment='top-left'
              gridColumn='1 / span 1'
              gridRow='2 / span 3'
            />
          </Pictures>
        </CenterLeft>
        <CenterRight>
          <VerticalTextBox
            title='O2. Clients'
            header='How did Teamify change their world.'
            description={description}
          />
        </CenterRight>
      </CenterSection>
      <BottomSection>
        <HorizontalTextBox
          title='O3. Reviews'
          header='What they say about us.'
          description={description}
        />
      </BottomSection>
    </>
  );
};

export default AboutUs;
