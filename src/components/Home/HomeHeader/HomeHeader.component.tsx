import React, { useState } from 'react';

import { Form } from '../../Form';
import { Navbar } from '../../Navbar';
import { HeaderContainer } from './HomeHeader.elements';
import circleImg from '../../../assets/landing/circle.png';
import { HeaderInner, HeaderLeft, HeaderTitle, HeaderText, CircleImage, Circle } from './HomeHeader.elements';

const HomeHeader = (): JSX.Element => {
  const [input, setInput] = useState<string>('');

  return (
    <HeaderContainer>
      <Navbar variant='mint' />
      <HeaderInner>
        <HeaderLeft>
          <HeaderTitle>Build a team in minutes!</HeaderTitle>
          <HeaderText>
            Find a dirrect link to some of the best developers and designers out there!
          </HeaderText>
          <Form
            placeholder='View user profiles and select your provider.'
            buttonLabel='Explore Now'
            labelText=''
            id=''
            value={input}
            variant='dark'
            onChange={(e) => setInput(e.currentTarget.value)}
            onSubmit={(e) => e.preventDefault()}
            buttonSize='lg'
          />
        </HeaderLeft>
      </HeaderInner>
      <CircleImage src={circleImg} alt='' />
      <Circle></Circle>
    </HeaderContainer>
  );
};

export default HomeHeader;
