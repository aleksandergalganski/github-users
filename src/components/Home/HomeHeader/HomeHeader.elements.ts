import styled from 'styled-components';

export const HeaderContainer = styled.header`
  height: auto;
  background-color: ${({ theme }) => theme.colors.mint};
  padding-bottom: 7rem;
  overflow: hidden;
  position: relative;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    padding-bottom: 3.125rem;
  }
`;
export const HeaderInner = styled.div`
  display: flex;
  width: 80%;
  margin: 0 auto;
  padding-top: 5rem;

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    width: 90%;
  }
`;

export const HeaderLeft = styled.div`
  flex-basis: 60%;

  @media (max-width: ${({ theme }) => theme.breakpoints.xl}) {
    flex-basis: 100%;
  }
`;

export const HeaderTitle = styled.h1`
  font-size: 80px;
  font-weight: ${({ theme }) => theme.fontWeights.bold};
  line-height: 1;
  letter-spacing: -1px;
  color: ${({ theme }) => theme.colors.dark2};
  padding-right: 1rem;
  padding-bottom: 1rem;

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    font-size: ${({ theme }) => theme.fontSizes.xl4};
  }
`;

export const HeaderText = styled.p`
  opacity: 0.7;
  font-size: ${({ theme }) => theme.fontSizes.lg};
  line-height: 1.78;
  color: ${({ theme }) => theme.colors.black};
  padding-right: 1rem;
  padding-bottom: 3rem;
`;

export const Circle = styled.div`
  position: absolute;
  width: 700px;
  height: 700px;
  border-radius: ${({ theme }) => theme.borderRadius.circle};
  top: 0;
  right: 0;
  background-color: ${({ theme }) => theme.colors.dark2};
  transform: translate(15%, 40%);

  @media (max-width: ${({ theme }) => theme.breakpoints.xl}) {
    display: none;
  }
`;

export const CircleImage = styled.img`
  position: absolute;
  width: 450px;
  height: 450px;
  bottom: 0px;
  right: 230px;
  z-index: 400;

  @media (max-width: ${({ theme }) => theme.breakpoints.xl}) {
    display: none;
  }
`;

