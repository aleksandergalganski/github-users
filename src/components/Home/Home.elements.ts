import styled from 'styled-components';

export const Container = styled.main`
  width: 90%;
  margin: 0 auto;
  min-height: 60vh;
  background-color: ${({ theme }) => theme.colors.white};
  padding-top: 4rem;
  padding-bottom: 5rem;

  @media (max-width: ${({ theme }) => theme.breakpoints.xl}) {
    width: 95%;
  }
`;