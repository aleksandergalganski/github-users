import React from 'react';

import { Testimonial } from './Testimonial';
import { TestimonialsContainer } from './Testimonials.elements';

const testimonial = {
  description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas aliquet orci, sit sagittis sed vel dictum lacus purus.  Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
  author: 'James Moore',
  company: 'CEO @ TheCompany'
};

const range = (from: number, to: number) => {
  const list: Array<number> = [];
  let counter = from;
  while (counter < to) {
    list.push(counter);
    counter++;
  }
  return list; 
};


const Testimonials = (): JSX.Element => {
  return (
    <TestimonialsContainer>
      {range(0, 3).map((num: number) => (
        <Testimonial 
          key={num} 
          description={testimonial.description} 
          author={testimonial.author} 
          company={testimonial.company} 
        />
      ))}
    </TestimonialsContainer>
  );
};

export default Testimonials;

