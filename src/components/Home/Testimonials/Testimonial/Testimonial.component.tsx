import React from 'react';

import {
  TestimonialAuthor,
  TestimonialAvatar,
  TestimonialDescription,
  TestimonialCompany,
  TestimonialContainer,
  TestimonialFooter,
  FooterLeft,
  FooterRight
} from './Testimonial.elements';

type TestimonialProps = {
  description: string;
  author: string;
  company: string;
};

const Testimonial = ({ description, author, company }: TestimonialProps): JSX.Element => {
  return (
    <TestimonialContainer>
      <TestimonialDescription>{description}</TestimonialDescription>
      <TestimonialFooter>
        <FooterLeft>
          <TestimonialAvatar></TestimonialAvatar>
        </FooterLeft>
        <FooterRight>
          <TestimonialAuthor>{author}</TestimonialAuthor>
          <TestimonialCompany>{company}</TestimonialCompany>
        </FooterRight>
      </TestimonialFooter>
    </TestimonialContainer>
  );
};

export default Testimonial;
