import styled from 'styled-components';

export const TestimonialContainer = styled.article`
  padding: 2.5rem;
  background-color: ${({ theme }) => theme.colors.lightGrey};
  max-width: 400px;

  &:nth-of-type(3) {
    @media (max-width: ${({ theme }) => theme.breakpoints.xl}) {
      grid-column: 1 / span 2;
      justify-self: center;
    }

    @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
      grid-column: auto;
    }
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    max-width: 100%;
  }
`;

export const TestimonialDescription = styled.p`
  margin-bottom: 3.75rem;
  font-size: ${({ theme }) => theme.fontSizes.md};
  line-height: 1.76;
  color: ${({ theme }) => theme.colors.text.grey2};
`;

export const TestimonialFooter = styled.div`
  display: flex;
  align-items: center;
`;

export const FooterLeft = styled.div`
  flex-basis: 30%;

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    flex-basis: 20%;
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.sm}) {
    flex-basis: 30%;
  }
`;

export const FooterRight = styled.div`
  flex-basis: 70%;
  display: flex;
  flex-direction: column;

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    flex-basis: 80%;
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.sm}) {
    flex-basis: 70%;
  }
`;

export const TestimonialAvatar = styled.div`
  width: 60px;
  height: 60px;
  border-radius: 50%;
  background-color: ${({ theme }) => theme.colors.grey2};
`;
export const TestimonialAuthor = styled.h5`
  opacity: 0.7;
  font-size: ${({ theme }) => theme.fontSizes.md};
  color: ${({ theme }) => theme.colors.text.dark};
  font-weight: ${({ theme }) => theme.fontWeights.bold};
`;

export const TestimonialCompany = styled.span`
  opacity: 0.7;
  font-size: ${({ theme }) => theme.fontSizes.md};
  color: ${({ theme }) => theme.colors.text.dark};
`;
