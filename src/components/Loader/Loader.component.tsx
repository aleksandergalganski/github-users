import React from 'react';

import { StyledLoader } from './Loader.elements';

const Loader = (): JSX.Element => {
  return <StyledLoader data-testid='loader'></StyledLoader>;
};

export default Loader;
