import styled, { keyframes } from 'styled-components';

const rotate = keyframes`
  from {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  to {
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }
`;

export const StyledLoader = styled.div`
  font-size: 10px;
  position: relative;
  text-indent: -9999em;
  border-top: 0.5rem solid ${({ theme }) => theme.colors.orange};
  border-right: 0.5rem solid ${({ theme }) => theme.colors.orange};
  border-bottom: 0.5rem solid ${({ theme }) => theme.colors.orange};
  border-left: 0.5rem solid transparent;
  -webkit-transform: translateZ(0);
  -ms-transform: translateZ(0);
  transform: translateZ(0);
  -webkit-animation: ${rotate} 1.1s infinite linear;
  animation: ${rotate} 1.1s infinite linear;

  &,
  &:after {
    border-radius: 50%;
    width: 4rem;
    height: 4rem;
  }
`;
