import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const HeaderContainer = styled.header`
  height: auto;
  min-height: 500px;
  background-color: ${({ theme }) => theme.colors.dark2};
  position: relative;
`;

export const HeaderInner = styled.div`
  max-width: 70%;
  margin: 0 auto;
  display: flex;
  padding: 90px 0;

  @media (max-width: ${({ theme }) => theme.breakpoints.xl}) {
    max-width: 80%;
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    max-width: 95%;
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    flex-direction: column;
    align-items: center;
    padding: 30px 0 90px 0;
  }
`;

export const HeaderLeft = styled.div`
  flex-basis: 80%;
  display: flex;

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    flex-direction: column;
    align-items: center;
  }
`;

export const HeaderRight = styled.div`
  flex-basis: 20%;
`;

export const UserAvatar = styled.img`
  width: 200px;
  height: 200px;
  border-radius: ${({ theme }) => theme.borderRadius.circle};
  margin-right: 40px;

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    width: 150px;
    height: 150px;
  }
`;

export const UserInfo = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Nickname = styled.h4`
  font-size: ${({ theme }) => theme.fontSizes.xl2};
  font-weight: ${({ theme }) => theme.fontWeights.bold};
  color: ${({ theme }) => theme.colors.text.grey};
`;

export const Username = styled.h1`
  font-size: 50px;
  font-weight: ${({ theme }) => theme.fontWeights.bold};
  line-height: 1.6;
  letter-spacing: -0.63px;
  color: ${({ theme }) => theme.colors.text.white};
`;

export const UserStats = styled.div`
  display: flex;
  margin-top: 30px;
`;

export const StatsItem = styled.div`
  opacity: 0.7;
  display: flex;
  align-items: center;
  font-size: ${({ theme }) => theme.fontSizes.md};
  color: ${({ theme }) => theme.colors.white};

  &:first-of-type {
    margin-right: 30px;
  }
`;

export const ButtonContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
`;

export const GithubLink = styled.a`
  width: 186px;
  height: 60px;
  font-size: ${({ theme }) => theme.fontSizes.md};
  font-weight: ${({ theme }) => theme.fontWeights.bold};
  padding: 16px 29px 14px;
  border-radius: ${({ theme }) => theme.borderRadius.sm};
  color: ${({ theme }) => theme.colors.white};
  cursor: pointer;
  display: inline-block;
  background-color: ${({ theme }) => theme.colors.dark3};

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    margin-top: 20px;
  }
`;

export const Icon = styled.img`
  width: 16px;
  height: 20px;
  margin-right: 16px;
`;

export const Tab = styled.div`
  width: 725px;
  height: auto;
  padding: 10px;
  border-radius: ${({ theme }) => theme.borderRadius.md};
  box-shadow: 0 44px 64px 0 rgba(0, 0, 0, 0.1);
  background-color: ${({ theme }) => theme.colors.white};
  position: absolute;
  bottom: -40px;
  left: 0;
  right: 0;
  margin: auto;
  display: flex;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    width: 600px;
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    width: 500px;
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.sm}) {
    width: 90vw;
  }
`;

type TabItemProps = {
  $active: boolean;
};

export const TabItem = styled(Link)<TabItemProps>`
  padding: 20px 80px;
  border-radius: ${({ theme }) => theme.borderRadius.sm};
  flex-basis: 50%;
  background-color: ${({ $active, theme }) =>
    $active ? theme.colors.grey : theme.colors.white};
  font-size: ${({ theme }) => theme.fontSizes.xl};
  font-weight: ${({ theme }) => theme.fontWeights.bold};
  color: ${({ theme }) => theme.colors.text.dark};
  border: none;
  outline: none;
  text-align: center;
  display: inline-block;
  cursor: pointer;
  transition: background-color 0.5s ease;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    padding: 15px 50px;
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    font-size: ${({ theme }) => theme.fontSizes.md};
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.sm}) {
    padding: 10px 20px;
    font-size: ${({ theme }) => theme.fontSizes.sm};
  }
`;
