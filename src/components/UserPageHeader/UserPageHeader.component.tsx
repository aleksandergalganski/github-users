import React from 'react';
import { useSelector } from 'react-redux';

import followingIcon from '../../assets/following-icon.svg';
import followersIcon from '../..//assets/followers-icon.svg';
import { Navbar } from '../Navbar';
import { useRouteMatch } from 'react-router-dom';
import { RootState } from '../../redux/reducers';
import { Loader } from '../Loader';
import { 
  HeaderContainer,
  HeaderInner,
  HeaderLeft,
  UserAvatar,
  UserInfo,
  Username,
  Nickname,
  StatsItem,
  UserStats,
  Icon,
  HeaderRight,
  Tab,
  TabItem,
  ButtonContainer,
  GithubLink
} from './UserPageHeader.elements';
import { LoaderContainer } from '../../styles/GlobalStyles';

const UserPageHeader = (): JSX.Element => {
  const match = useRouteMatch<{ section: string }>('/users/:login/:section');
  const userDetails = useSelector((state: RootState) => state.user.userDetails.data);
  const userDetailsLoading = useSelector((state: RootState) => state.user.userDetails.isLoading);
  
  return (
    <HeaderContainer>
      <Navbar variant='grey' />
      {userDetailsLoading || !userDetails ? (
        <LoaderContainer>
          <Loader />
        </LoaderContainer>
      ) : (
        <>
          <HeaderInner>
            <HeaderLeft>
              <UserAvatar src={userDetails.avatar_url} alt={userDetails.login} />
              <UserInfo>
                <Username>{userDetails.login}</Username>
                <Nickname>Nickname</Nickname>
                <UserStats>
                  <StatsItem>
                    <Icon src={followingIcon} alt='following icon' />
                    <span>Following ({userDetails.following})</span>
                  </StatsItem>
                  <StatsItem>
                    <Icon src={followersIcon} alt='followers icon' />
                    <span>Followers ({userDetails.followers})</span>
                  </StatsItem>
                </UserStats>
              </UserInfo>
            </HeaderLeft>
            <HeaderRight>
              <ButtonContainer>
                <GithubLink href={userDetails.html_url} target='_blank'>
                  View on GitHub
                </GithubLink>
              </ButtonContainer>
            </HeaderRight>
          </HeaderInner>
          <Tab>
            <TabItem
              to={`/users/${userDetails.login}/repositories`}
              $active={match?.params.section === 'repositories'}
            >
              Repositories ({userDetails.public_repos})
            </TabItem>
            <TabItem
              to={`/users/${userDetails.login}/followers`}
              $active={match?.params.section === 'followers'}
            >
              Followers ({userDetails.followers})
            </TabItem>
          </Tab>
        </>
      )}
    </HeaderContainer>
  );
};

export default UserPageHeader;
