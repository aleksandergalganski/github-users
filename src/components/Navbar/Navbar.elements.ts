import styled, { css } from 'styled-components';
import { FaBars } from 'react-icons/fa';
import { Link } from 'react-router-dom';

export const NavbarContainer = styled.div`
  width: 90%;
  margin: 0 auto;
  padding: 0 50px;
  overflow: hidden;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    width: 95%;
    padding: 0 10px;
  }
`;

export const Nav = styled.nav`
  width: 100%;
`;

export const NavInner = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 25px 0;
`;

export const NavLeft = styled.div``;

type NavRightProps = {
  variant: 'grey' | 'dark' | 'mint';
  open: boolean;
};

export const NavRight = styled.div<NavRightProps>`
  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    position: fixed;
    top: 0;
    width: 100vw;
    height: 100vh;
    right: 0;
    left: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    z-index: 1500;
    padding-bottom: 30px;
    transition: transform 0.5s ease;
    transform: ${({ open }) => (open ? 'translateX(0)' : 'translateX(100%)')};

    ${({ variant }) =>
    variant === 'mint' &&
      css`
        background-color: ${({ theme }) => theme.colors.mint};
      `};

    ${({ variant }) =>
    variant === 'grey' &&
      css`
        background-color: ${({ theme }) => theme.colors.dark2};
      `};

    ${({ variant }) =>
    variant === 'dark' &&
      css`
        background-color: ${({ theme }) => theme.colors.grey};
      `};
  }
`;

export const NavList = styled.div`
  display: flex;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    flex-direction: column;
  }
`;

export const AppTitle = styled(Link)`
  color: ${({ theme }) => theme.colors.text.dark3};
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const AppLogo = styled.img``;

type NavLinkProps = {
  variant: 'grey' | 'dark' | 'mint';
};

export const NavLink = styled(Link)<NavLinkProps>`
  color: ${({ theme, variant }) =>
    variant === 'grey' ? theme.colors.text.white : theme.colors.text.dark};
  font-weight: ${({ theme }) => theme.fontWeights.bold};
  font-size: ${({ theme }) => theme.fontSizes.lg};
  padding: 8px 12px;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    color: ${({ theme, variant }) => variant === 'grey' ? theme.colors.text.white : theme.colors.text.dark};
  }
`;

type DividerProps = {
  variant: 'grey' | 'dark' | 'mint';
};

export const Divider = styled.div<DividerProps>`
  background-color: ${({ theme, variant }) => variant === 'grey' ? '#cbcbcb' : theme.colors.dark2};
  width: 100%;
  height: 1px;
  opacity: 0.15;
`;

type BarsButton = {
  open: boolean;
};

export const BarsButton = styled.button<BarsButton>`
  border: none;
  outline: none;
  background: none;
  display: none;
  cursor: pointer;
  z-index: 1600;
  
  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    display: block;
    position: sticky;
  }
`;

type BarsIconProps = {
  variant: 'grey' | 'dark' | 'mint';
};

export const BarsIcon = styled(FaBars)<BarsIconProps>`
  font-size: ${({ theme }) => theme.fontSizes.lg};
  color: ${({ theme, variant }) =>
    variant === 'grey' ? theme.colors.text.white : theme.colors.text.dark};
`;