import React, { useState, useEffect } from 'react';

import appLogoDark from '../../assets/teamifyDark.png';
import appLogoLight from '../../assets/teamifyLight.svg';
import { 
  Nav, 
  NavbarContainer, 
  NavInner, 
  NavLeft, 
  NavRight, 
  AppTitle, 
  AppLogo, 
  NavLink, 
  BarsButton,
  BarsIcon,
  NavList,
  Divider 
} from './Navbar.elements';

const navLinks: Array<{ to: string; label: string }> = [
  { to: '/users', label: 'Freelancers' },
  { to: '/', label: 'About Us' },
  { to: '/', label: 'Case study' },
  { to: '/', label: 'Reviews' },
  { to: '/', label: 'Newsletter' }
];

type NavbarProps = {
  variant?: 'grey' | 'dark' | 'mint';
};

const Navbar = ({ variant = 'dark' }: NavbarProps): JSX.Element => {
  const [menuOpen, setMenuOpen] = useState(false);

  useEffect(() => {
    if (menuOpen) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'auto';
    }
  }, [menuOpen]);

  return (
    <Nav>
      <NavbarContainer>
        <NavInner>
          <NavLeft>
            <AppTitle to='/'>
              <AppLogo 
                src={variant === 'dark' || variant === 'mint' ? appLogoDark : appLogoLight} 
                alt='App logo' 
              />
            </AppTitle>
          </NavLeft>
          <BarsButton onClick={() => setMenuOpen(prev => !prev)} open={menuOpen}>
            <BarsIcon variant={variant} />
          </BarsButton>
          <NavRight open={menuOpen} variant={variant}>
            <NavList>
              {navLinks.map(({ to, label }) => (
                <NavLink key={label} to={to} variant={variant}>
                  {label}
                </NavLink>
              ))}
            </NavList>
          </NavRight>
        </NavInner>
        <Divider variant={variant} />
      </NavbarContainer>
    </Nav>
  );
};

export default Navbar;