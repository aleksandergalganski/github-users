import React from 'react';
import { Link } from 'react-router-dom';

import { User as IUser } from '../../../models/User';
import { Card, CardBody, Avatar, Login, Nickname } from './UserCard.elements';

type UserCardProps = {
  user: IUser;
};

const UserCard = ({ user }: UserCardProps): JSX.Element => {
  return (
    <Link to={`/users/${user.login}/repositories`} data-testid='user-card'>
      <Card>
        <CardBody>
          <Avatar src={user.avatar_url} alt='user photo' />
          <Login>{user.login}</Login>
          <Nickname>Nickname</Nickname>
        </CardBody>
      </Card>
    </Link>
  );
};

export default UserCard;
