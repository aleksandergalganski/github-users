import styled from 'styled-components';

export const Card = styled.div`
  width: auto;
  min-width: 220px;
  height: 300px;
  border-radius: ${({ theme }) => theme.borderRadius.md};
  border: solid 1px ${({ theme }) => theme.colors.grey};
`;

export const CardBody = styled.div`
  width: 100%;
  height: 100%;
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 40px 20px 20px 20px;
`;

export const Avatar = styled.img`
  width: 150px;
  height: 150px;
  border-radius: ${({ theme }) => theme.borderRadius.circle};
`;

export const Login = styled.h4`
  margin: 10px 4px;
  opacity: 0.7;
  font-size: ${({ theme }) => theme.fontSizes.xl2};
  font-weight: ${({ theme }) => theme.fontWeights.bold};
  line-height: 1.25;
  text-align: center;
  color: ${({ theme }) => theme.colors.text.dark};
`;

export const Nickname = styled.h5`
  opacity: 0.7;
  font-size: ${({ theme }) => theme.fontSizes.md};
  text-align: center;
  color: ${({ theme }) => theme.colors.text.dark};
`;
