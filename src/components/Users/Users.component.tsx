import React from 'react';

import { UserList } from './Users.elements';
import { UserCard } from './UserCard';
import { User as IUser } from '../../models/User';

type UsersProps = {
  users: Array<IUser>;
};

const Users = ({ users = [] }: UsersProps): JSX.Element => {
  return (
    <UserList>
      {users.map((user: IUser) => <UserCard key={user.id} user={user} />)}
    </UserList>
  );
};

export default Users;
