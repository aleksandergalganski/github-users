import styled from 'styled-components';

export const FooterContainer = styled.footer`
  width: 100%;
  height: auto;
  padding: 2.5rem 1.9rem;
  background-color: ${({ theme }) => theme.colors.mint};

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    padding: 2.5rem 1rem;
  }
`;

export const FooterTop = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 3.75rem 0;
`;

export const FooterHeading = styled.h2`
  font-size: 3rem;
  letter-spacing: -1px;
  font-weight: ${({ theme }) => theme.fontWeights.bold};
  color: ${({ theme }) => theme.colors.text.dark};
  margin-bottom: 0.625rem;

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    font-size: 2rem;
    line-height: 1.2;
    text-align: center;
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.sm}) {
    font-size: 1.5rem;
  }
`;

export const FooterText = styled.p`
  font-size: ${({ theme }) => theme.fontSizes.lg};
  opacity: 0.7;
  color: ${({ theme }) => theme.colors.text.dark};
  font-weight: ${({ theme }) => theme.fontWeights.normal};
  margin-bottom: 4.375rem;

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    text-align: center;  
  }
`;

export const FormContainer = styled.div`
  width: 50%;
  margin: 0 auto;

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    width: 90%;
  }
`;

export const FooterBottom = styled.div`
  max-width: 95%;
  margin: 0 auto;
  display: flex;
  align-items: center;

  & > * {
    flex-basis: 100%;
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    flex-direction: column;

    & > * {
      margin-bottom: 1.25rem;
    }
  }
`;

export const AppName = styled.div`
  font-size: ${({ theme }) => theme.fontSizes.xl};
  font-weight: ${({ theme }) => theme.fontWeights.bold};
  color: ${({ theme }) => theme.colors.text.dark};
`;

export const CopyrightSection = styled.p`
  font-size: ${({ theme }) => theme.fontSizes.lg};
  text-align: center;
  color: ${({ theme }) => theme.colors.text.grey2};

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    line-height: 1.2;
  }
`;

export const ImageContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: flex-end;

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    justify-content: center;
  }
`;

export const Logo = styled.img`
  width: 15px;
  height: 15px;
`;
