import React, { useState } from 'react';

import { Form } from '../Form';
import twitterLogo from '../../assets/twitter.svg';
import {
  FooterContainer,
  FooterTop,
  FooterHeading,
  FooterText,
  FormContainer,
  FooterBottom,
  AppName,
  CopyrightSection,
  ImageContainer,
  Logo
} from './Footer.elements';

const Footer = (): JSX.Element => {
  const [searchQuery, setSearchQuery] = useState<string>('');

  const onSubmit = (e: React.SyntheticEvent) => {
    e.preventDefault();
  };

  return (
    <FooterContainer>
      <FooterTop>
        <FooterHeading>Subscribe to get instant alerts</FooterHeading>
        <FooterText>
          Don’t wanna miss something? Subscribe right now and get special deals and
          monthly newsletter
        </FooterText>
        <FormContainer>
          <Form
            value={searchQuery}
            id='email-address'
            placeholder='Enter your email address'
            buttonLabel='Submit'
            labelText='Email address'
            variant='light'
            onSubmit={onSubmit}
            onChange={(e: React.FormEvent<HTMLInputElement>) =>
              setSearchQuery(e.currentTarget.value)
            }
          />
        </FormContainer>
      </FooterTop>
      <FooterBottom>
        <AppName>© Teamify</AppName>
        <CopyrightSection>Copyrights @ Teamify, All rights reserved</CopyrightSection>
        <ImageContainer>
          <Logo src={twitterLogo} alt='Twitter logo' />
        </ImageContainer>
      </FooterBottom>
    </FooterContainer>
  );
};

export default Footer;
