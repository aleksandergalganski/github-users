import React from 'react';

import { StyledForm, Label, Input, Button } from './Form.elements';

type FormProps = {
  placeholder: string;
  labelText: string;
  id: string;
  value: string;
  buttonLabel: string;
  variant: 'light' | 'dark';
  buttonSize?: 'sm' | 'lg';
  onChange: (e: React.FormEvent<HTMLInputElement>) => void;
  onSubmit: (e: React.SyntheticEvent) => void;
};

const Form = ({
  placeholder,
  labelText,
  variant,
  id,
  value,
  buttonLabel,
  buttonSize,
  onChange,
  onSubmit
}: FormProps): JSX.Element => {
  return (
    <StyledForm onSubmit={onSubmit}>
      <Label srOnly htmlFor={id}>
        {labelText}
      </Label>
      <Input
        variant={variant}
        placeholder={placeholder}
        value={value}
        name={id}
        id={id}
        onChange={onChange}
      />
      <Button type='submit' size={buttonSize}>
        {buttonLabel}
      </Button>
    </StyledForm>
  );
};

export default Form;
