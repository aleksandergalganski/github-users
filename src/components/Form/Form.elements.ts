import styled, { css } from 'styled-components';

export const StyledForm = styled.form`
  width: 100%;
  background-color: ${({ theme }) => theme.colors.white};
  padding: 1rem;
  border-radius: ${({ theme }) => theme.borderRadius.md};
  box-shadow: 0 44px 64px 0 rgba(0, 0, 0, 0.1);
  display: flex;
  align-items: center;
  position: sticky;
  z-index: 1000;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    margin-top: 10px;
    padding: 8px;
  }
`;

interface InputProps {
  variant: 'dark' | 'light';
}

export const Input = styled.input<InputProps>`
  font-size: ${({ theme }) => theme.fontSizes.lg};
  font-weight: ${({ theme }) => theme.fontWeights.bold};
  color: ${({ theme }) => theme.colors.text.dark};
  border: none;
  outline: none;
  width: 100%;
  padding: 8px 12px;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    font-size: ${({ theme }) => theme.fontSizes.md};
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    font-size: ${({ theme }) => theme.fontSizes.sm};
  }

  ${(props) =>
    props.variant === 'dark' &&
    css`
      color: ${({ theme }) => theme.colors.text.dark};
      &::placeholder {
        color: ${({ theme }) => theme.colors.text.dark};
      }
    `}

  ${(props) =>
    props.variant === 'light' &&
    css`
      opacity: 0.7;
      color: ${({ theme }) => theme.colors.text.dark};
      font-weight: ${({ theme }) => theme.fontWeights.normal};

      &::placeholder {
        opacity: 0.7;
        color: ${({ theme }) => theme.colors.text.dark};
        font-weight: ${({ theme }) => theme.fontWeights.normal};
      }
    `}
`;

interface LabelProps {
  srOnly?: boolean;
}

export const Label = styled.label<LabelProps>`
  ${(props) =>
    props.srOnly &&
    css`
      border: 0 !important;
      clip: rect(1px, 1px, 1px, 1px) !important;
      -webkit-clip-path: inset(50%) !important;
      clip-path: inset(50%) !important;
      height: 1px !important;
      margin: -1px !important;
      overflow: hidden !important;
      padding: 0 !important;
      position: absolute !important;
      width: 1px !important;
      white-space: nowrap !important;
    `}
`;

type ButtonProps = {
  size?: 'sm' | 'lg';
}

export const Button = styled.button<ButtonProps>`
  padding: 1.25rem 2rem;
  border-radius: ${({ theme }) => theme.borderRadius.sm};
  text-align: center;
  background-color: ${({ theme }) => theme.colors.dark};
  color: ${({ theme }) => theme.colors.white};
  font-size: ${({ theme }) => theme.fontSizes.lg};
  font-weight: ${({ theme }) => theme.fontWeights.semibold};
  display: inline-block;
  cursor: pointer;
  border: none;

  ${(props) => props.size === 'lg' && css`
    width: 260px;
  `};

  &:hover {
    background-color: ${({ theme }) => theme.colors.dark3};
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    padding: 12px 20px;
    font-size: ${({ theme }) => theme.fontSizes.md};
  }
`;
