import React from 'react';
import { useSelector } from 'react-redux';

import { RootState } from '../../redux/reducers';
import { Loader } from '../Loader';
import { LoaderContainer } from '../../styles/GlobalStyles';
import { Users } from '../Users';

const Followers = (): JSX.Element => {
  const followers = useSelector((state: RootState) => state.user.followers.data);
  const followersLoading = useSelector((state: RootState) => state.user.followers.isLoading);
  const followersError = useSelector((state: RootState) => state.user.followers.error);

  if (followersError) {
    return <p>Something wrong happened while fetching repos. Try again later.</p>;
  }

  if (followersLoading || !followers) {
    return (
      <LoaderContainer>
        <Loader />
      </LoaderContainer>
    );
  }

  return (
    <Users users={followers} />
  );
};

export default Followers;
