import React from 'react';

import { Repository } from '../../../models/Repository';
import { 
  RepoContainer, 
  RepoInner, 
  Avatar,
  RepoInfo, 
  RepoName, 
  ForkInfo, 
  RepoDescription 
} from './Repo.elements';

type RepoProps = {
  repoDetails: Repository
};

const Repo = ({ repoDetails: { 
  html_url, 
  name, 
  owner, 
  description,
  fork, 
  forks_url 
}}: RepoProps): JSX.Element => {
  return (
    <RepoContainer href={html_url} target='_blank' rel='noreferrer' data-testid='repo'>
      <RepoInner>
        <Avatar src={owner?.avatar_url} alt={owner?.login} />
        <RepoInfo>
          <RepoName>{name}</RepoName>
          {fork && <ForkInfo>Forked from {forks_url}</ForkInfo>}
          <RepoDescription>{description}</RepoDescription>
        </RepoInfo>
      </RepoInner>
    </RepoContainer>
  );
};

export default Repo;
