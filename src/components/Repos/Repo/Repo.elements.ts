import styled from 'styled-components';

export const RepoContainer = styled.a`
  display: block;
  width: 100%;
  height: 250px;
  padding: 50px;
  border-radius: ${({ theme }) => theme.borderRadius.md};
  border: solid 1px ${({ theme }) => theme.colors.grey};

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    padding: 10px;
  }
`;

export const RepoInner = styled.div`
  display: flex;
`;

export const Avatar = styled.img`
  width: 150px;
  height: 150px;
  border-radius: ${({ theme }) => theme.borderRadius.circle};
  margin-right: 40px;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    width: 120px;
    height: 120px;
    margin-right: 10px;
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.sm}) {
    width: 80px;
    height: 80px;
    margin-right: 10px;
  }
`;

export const RepoInfo = styled.div`
  display: flex;
  flex-direction: column;
`;

export const RepoName = styled.h3`
  opacity: 0.7;
  font-size: ${({ theme }) => theme.fontSizes.xl7};
  font-weight: ${({ theme }) => theme.fontWeights.bold};
  color: ${({ theme }) => theme.colors.text.dark};

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    font-size: ${({ theme }) => theme.fontSizes.xl};
  }
`;

export const ForkInfo = styled.p`
  opacity: 0.7;
  font-size: ${({ theme }) => theme.fontSizes.md};
  color: ${({ theme }) => theme.colors.text.dark};
  margin-bottom: 16px;
`;

export const RepoDescription = styled.p`
  opacity: 0.7;
  font-size: ${({ theme }) => theme.fontSizes.xl2};
  font-weight: ${({ theme }) => theme.fontWeights.bold};
  color: ${({ theme }) => theme.colors.text.dark};

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    font-size: ${({ theme }) => theme.fontSizes.md};
  }
`;
