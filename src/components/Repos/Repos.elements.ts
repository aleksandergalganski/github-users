import styled from 'styled-components';

export const ReposList = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 2rem;
  width: 100%;
  padding-top: 100px;
  padding-bottom: 60px;

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    justify-items: center;
  }
`;
