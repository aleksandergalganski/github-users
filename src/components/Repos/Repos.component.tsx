import React from 'react';
import { useSelector } from 'react-redux';

import { RootState } from '../../redux/reducers';
import { Repo } from './Repo';
import { Loader } from '../Loader';
import { Repository } from '../../models/Repository';
import { ReposList } from './Repos.elements';
import { LoaderContainer } from '../../styles/GlobalStyles';

const Repos = (): JSX.Element => {
  const repos = useSelector((state: RootState) => state.user.repos.data);
  const reposLoading = useSelector((state: RootState) => state.user.repos.isLoading);
  const reposError = useSelector((state: RootState) => state.user.repos.error);
  
  if (reposError) {
    return <p>Something wrong happened while fetching repos. Try again later.</p>;
  }

  if (reposLoading || !repos) {
    return (
      <LoaderContainer>
        <Loader />
      </LoaderContainer>
    );
  }

  return (
    <ReposList>
      {repos.map((repo: Repository) => (
        <Repo key={repo.id} repoDetails={repo} />
      ))}
    </ReposList>
  );
};

export default Repos;
