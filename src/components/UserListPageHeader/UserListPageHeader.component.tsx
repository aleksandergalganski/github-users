import React, { useState } from 'react';
import { useHistory } from 'react-router';

import { Navbar } from '../Navbar';
import { Form } from '../Form';
import { HeaderContainer, HeaderInner, HeaderTitle } from './UserListPageHeader.elements';

const Header = (): JSX.Element => {
  const [searchQuery, setSearchQuery] = useState<string>('');
  const history = useHistory();

  const onSubmit = (e: React.SyntheticEvent) => {
    e.preventDefault();
    if (searchQuery !== '') {
      history.push(`/users?q=${searchQuery}`);
    }
  };

  return (
    <HeaderContainer>
      <Navbar variant='dark' />
      <HeaderInner>
        <HeaderTitle>Meet your future team!</HeaderTitle>
        <Form 
          value={searchQuery}
          id='search-users'
          placeholder='Search by nickname'
          buttonLabel='Search'
          labelText='Search users'
          onSubmit={onSubmit}
          variant='dark'
          onChange={(e: React.FormEvent<HTMLInputElement>) => setSearchQuery(e.currentTarget.value)}
        />
      </HeaderInner>
    </HeaderContainer>
  );
};

export default Header;
