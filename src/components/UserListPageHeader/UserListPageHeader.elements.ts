import styled from 'styled-components';

export const HeaderContainer = styled.header`
  height: auto;
  background-color: ${({ theme }) => theme.colors.grey};
  padding-bottom: 100px;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    padding-bottom: 50px;
  }
`;

export const HeaderInner = styled.div`
  width: 80%;
  margin: 0 auto;
  margin-top: 60px;

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    max-width: 90%;
    margin-top: 40px;
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    margin-top: 30px;
  }
`;

export const HeaderTitle = styled.h1`
  color: ${({ theme }) => theme.colors.text.dark2};
  font-weight: ${({ theme }) => theme.fontWeights.bold};
  font-size: 80px;
  letter-spacing: -1px;
  padding-bottom: 1rem;

  @media (max-width: ${({ theme }) => theme.breakpoints.xl}) {
    font-size: 60px;
    text-align: center;
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.lg}) {
    font-size: 40px;
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    font-size: 25px;
  }
`;
