import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import styled from 'styled-components';

import { getUserFollowers, getUserRepos, getUserDetails } from '../redux/actions/userActions';
import { Footer } from '../components/Footer';
import { UserPageHeader } from '../components/UserPageHeader';
import { Repos } from '../components/Repos';
import { Followers } from '../components/Followers';

const Container = styled.div`
  width: 80%;
  margin: 0 auto;

  @media (max-width: ${({ theme }) => theme.breakpoints.xl}) {
    width: 90%;
  }

  @media (max-width: ${({ theme }) => theme.breakpoints.md}) {
    width: 95%;
  }
`;

const User = ({ match }: RouteComponentProps<{ login: string; section: string; }>): JSX.Element => {
  const dispatch = useDispatch();

  useEffect(() => {
    const username = match.params.login;
    dispatch(getUserFollowers(username));
    dispatch(getUserRepos(username));
    dispatch(getUserDetails(username));
  }, [dispatch, match.params.login]);

  return (
    <>
      <UserPageHeader />
      <Container>
        {match.params.section === 'repositories' ? <Repos /> : <Followers />} 
      </Container>
      <Footer />
    </>
  );
};

export default User;
