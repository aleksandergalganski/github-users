import React from 'react';

import { UserListPageHeader } from '../components/UserListPageHeader';
import { Freelancers } from '../components/Freelancers';
import { Footer } from '../components/Footer';

const Users = (): JSX.Element => {
  return (
    <>
      <UserListPageHeader />
      <Freelancers />
      <Footer />
    </>
  );
};

export default Users;
