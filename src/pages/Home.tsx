import React from 'react';

import { Home } from '../components/Home';
import { Footer } from '../components/Footer';

const HomePage = (): JSX.Element => {
  return (
    <>
      <Home />
      <Footer />
    </>
  );
};

export default HomePage;
