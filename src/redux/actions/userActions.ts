import { Action, AnyAction } from 'redux';
import githubApi from '../../api/githubApi';
import { Repository } from '../../models/Repository';
import { User } from '../../models/User';
import { UserDetails } from '../../models/UserDetails';
import { AppThunk } from './index';
import {
  GET_FOLLOWERS_FAILURE,
  GET_FOLLOWERS_SUCCESS,
  GET_FOLLOWERS_REQUEST,
  GET_REPOS_SUCCESS,
  GET_REPOS_FAILURE,
  GET_REPOS_REQUEST,
  GET_USER_DETAILS_FAILURE,
  GET_USER_DETAILS_SUCCESS,
  GET_USER_DETAILS_REQUEST
} from './types/userActions';

export const getUserRepos = (username: string): AppThunk => async (dispatch) => {
  try {
    dispatch({ type: GET_REPOS_REQUEST });
    const response = await githubApi.get<Array<Repository>>(`/users/${username}/repos`);
    dispatch({ type: GET_REPOS_SUCCESS, payload: response.data });
  } catch (err) {
    const error = err.response ? err.response.data.message : err.message;
    dispatch({ type: GET_REPOS_FAILURE, payload: error });
  }
};

export const getUserFollowers =
  (username: string): AppThunk | Action =>
    async (dispatch) => {
      try {
        dispatch({ type: GET_FOLLOWERS_REQUEST });
        const response = await githubApi.get<Array<User>>(`/users/${username}/followers`);
        dispatch({ type: GET_FOLLOWERS_SUCCESS, payload: response.data });
      } catch (err) {
        const error = err.response ? err.response.data.message : err.message;
        dispatch({ type: GET_FOLLOWERS_FAILURE, payload: error });
      }
    };

export const getUserDetails = (username: string): AppThunk | Action => async (dispatch) => {
  try {
    dispatch({ type: GET_USER_DETAILS_REQUEST });
    const response = await githubApi.get<UserDetails>(`/users/${username}`);
    dispatch({ type: GET_USER_DETAILS_SUCCESS, payload: response.data });
  } catch (err) {
    const error = err.response ? err.response.data.message : err.message;
    dispatch({ type: GET_USER_DETAILS_FAILURE, payload: error });
  }
};