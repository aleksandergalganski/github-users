import { User } from '../../../models/User';

export const SEARCH_USERS_REQUEST = 'SEARCH_USERS_REQUEST';
export const SEARCH_USERS_SUCCESS = 'SEARCH_USERS_SUCCESS';
export const SEARCH_USERS_FAILURE = 'SEARCH_USERS_FAILURE';

interface SearchUsersRequest {
  type: typeof SEARCH_USERS_REQUEST
}

interface SearchUsersSuccess {
  type: typeof SEARCH_USERS_SUCCESS;
  payload: {
    total_count: number;
    incomplete_results: boolean;
    items: Array<User>;
  };
}

interface SearchUsersFailure {
  type: typeof SEARCH_USERS_FAILURE,
  payload: string
}

export type UsersAction = 
  | SearchUsersRequest
  | SearchUsersSuccess
  | SearchUsersFailure;