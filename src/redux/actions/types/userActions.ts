import { Repository } from '../../../models/Repository';
import { User } from '../../../models/User';
import { UserDetails } from '../../../models/UserDetails';

export const GET_REPOS_REQUEST = 'GET_REPOS_REQUEST';
export const GET_REPOS_SUCCESS = 'GET_REPOS_SUCCESS';
export const GET_REPOS_FAILURE = 'GET_REPOS_FAILURE';

export const GET_FOLLOWERS_REQUEST = 'GET_FOLLOWERS_REQUEST';
export const GET_FOLLOWERS_SUCCESS = 'GET_FOLLOWERS_SUCCESS';
export const GET_FOLLOWERS_FAILURE = 'GET_FOLLOWERS_FAILURE';

export const GET_USER_DETAILS_REQUEST = 'GET_USER_DETAILS_REQUEST';
export const GET_USER_DETAILS_SUCCESS = 'GET_USER_DETAILS_SUCCESS';
export const GET_USER_DETAILS_FAILURE = 'GET_USER_DETAILS_FAILURE';

interface GetReposRequest {
  type: typeof GET_REPOS_REQUEST
}

interface GetReposSuccess {
  type: typeof GET_REPOS_SUCCESS;
  payload: Array<Repository>
}

interface GetReposFailure {
  type: typeof GET_REPOS_FAILURE;
  payload: string
}

interface GetFollwersRequest {
  type: typeof GET_FOLLOWERS_REQUEST
}

interface GetFollowerSuccess {
  type: typeof GET_FOLLOWERS_SUCCESS;
  payload: Array<User>
}

interface GetFollwersFailure {
  type: typeof GET_FOLLOWERS_FAILURE;
  payload: string;
}

interface GetUserDetailsRequest {
  type: typeof GET_USER_DETAILS_REQUEST
}

interface GetUserDetailsSuccess {
  type: typeof GET_USER_DETAILS_SUCCESS;
  payload: UserDetails;
}

interface GetUserDetailsFailure {
  type: typeof GET_USER_DETAILS_FAILURE;
  payload: string;
}

export type ReposAction = GetReposRequest | GetReposSuccess | GetReposFailure;

export type FollowersAction = GetFollwersRequest | GetFollowerSuccess | GetFollwersFailure;

export type UserDetailsAction = GetUserDetailsRequest | GetUserDetailsSuccess | GetUserDetailsFailure;
