import githubApi from '../../api/githubApi';
import {
  SEARCH_USERS_FAILURE,
  SEARCH_USERS_SUCCESS,
  SEARCH_USERS_REQUEST
} from './types/usersActions';
import { AppThunk } from './index';

const usersPerPage = 12;

export const searchUsers = ({ q, page = 1 }: { q: string; page?: number }): AppThunk => async (dispatch) => {
  try {
    dispatch({ type: SEARCH_USERS_REQUEST });
    const response = await githubApi.get(`/search/users?q=${q}&page=${page}&per_page=${usersPerPage}`);
    dispatch({ type: SEARCH_USERS_SUCCESS, payload: response.data });
  } catch (err) {
    const error = err.response ? err.response.data.message : err.message;
    dispatch({ type: SEARCH_USERS_FAILURE, payload: error });
  }
};