import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import { usersReducer, userReducer } from './reducers';

const rootReducer = combineReducers({
  users: usersReducer,
  user: userReducer
});

const initialState = {};

const store = createStore(
  rootReducer, 
  initialState,
  composeWithDevTools(applyMiddleware(thunk))
);

export default store;
export {
  rootReducer
};