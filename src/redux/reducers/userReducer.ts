import { combineReducers } from 'redux';

import { Repository } from '../../models/Repository';
import { User } from '../../models/User';
import { UserDetails } from '../../models/UserDetails';
import {
  GET_FOLLOWERS_FAILURE,
  GET_FOLLOWERS_REQUEST,
  GET_FOLLOWERS_SUCCESS,
  GET_REPOS_SUCCESS,
  GET_REPOS_FAILURE,
  GET_REPOS_REQUEST,
  GET_USER_DETAILS_FAILURE,
  GET_USER_DETAILS_SUCCESS,
  GET_USER_DETAILS_REQUEST,
  ReposAction,
  FollowersAction,
  UserDetailsAction
} from '../actions/types/userActions';

export interface ReposState {
  data: Array<Repository>,
  isLoading: boolean,
  error: string |null
}

const reposeInitialState: ReposState = {
  data: [],
  isLoading: false,
  error: null
};

const reposReducer = (
  state: ReposState = reposeInitialState,
  action: ReposAction
): ReposState => {
  switch (action.type) {
  case GET_REPOS_REQUEST:
    return {
      ...state,
      isLoading: true
    };
  case GET_REPOS_SUCCESS:
    return {
      ...state,
      isLoading: false,
      data: action.payload
    };
  case GET_REPOS_FAILURE:
    return {
      ...state,
      error: action.payload,
      isLoading: false
    };
  default: 
    return state;
  }
};


export interface FollowersState {
  data: Array<User>,
  isLoading: boolean,
  error: string | null
}

const followersInitialState: FollowersState = {
  data: [],
  isLoading: false,
  error: null
};

export const followersReducer = (
  state: FollowersState = followersInitialState,
  action: FollowersAction
): FollowersState => {
  switch (action.type) {
  case GET_FOLLOWERS_REQUEST:
    return {
      ...state,
      isLoading: true,
    };
  case GET_FOLLOWERS_SUCCESS:
    return {
      ...state,
      isLoading: false,
      data: action.payload,
    };
  case GET_FOLLOWERS_FAILURE:
    return {
      ...state,
      error: action.payload,
      isLoading: false,
    };
  default:
    return state;
  }
};

export interface UserDetailsState {
  data: UserDetails | null,
  isLoading: boolean,
  error: string | null
}

const userDetailsInitialState: UserDetailsState = {
  data: null,
  isLoading: false,
  error: null,
};

const userDetailsReducer = (
  state: UserDetailsState = userDetailsInitialState,
  action: UserDetailsAction,
): UserDetailsState => {
  switch (action.type) {
  case GET_USER_DETAILS_REQUEST:
    return {
      ...state,
      isLoading: true,
    };
  case GET_USER_DETAILS_SUCCESS:
    return {
      ...state,
      isLoading: false,
      data: action.payload,
    };
  case GET_USER_DETAILS_FAILURE:
    return {
      ...state,
      error: action.payload,
      isLoading: false,
    };
  default:
    return state;
  }
};


const userReducer = combineReducers({
  repos: reposReducer,
  followers: followersReducer,
  userDetails: userDetailsReducer
});

export default userReducer;