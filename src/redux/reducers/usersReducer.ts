import {
  UsersAction,
  SEARCH_USERS_FAILURE,
  SEARCH_USERS_REQUEST,
  SEARCH_USERS_SUCCESS
} from '../actions/types/usersActions';
import { User } from '../../models/User';

export interface UsersState {
  data: Array<User>;
  totalCount: number | null;
  incompleteResults: boolean | null;
  isLoading: boolean;
  error: null | string;
}

const initialState: UsersState = {
  data: [],
  isLoading: false,
  error: null,
  totalCount: null,
  incompleteResults: null,
};

const usersReducer = (
  state: UsersState = initialState,
  action: UsersAction
): UsersState => {
  switch (action.type) {
  case SEARCH_USERS_REQUEST:
    return {
      ...state,
      isLoading: true
    };
  case SEARCH_USERS_SUCCESS: {
    return {
      ...state,
      isLoading: false,
      totalCount: action.payload.total_count,
      incompleteResults: action.payload.incomplete_results,
      data: action.payload.items,
    };
  }
  case SEARCH_USERS_FAILURE:
    return {
      ...state,
      isLoading: false,
      error: action.payload
    };
  default: 
    return state;
  }
};

export default usersReducer;