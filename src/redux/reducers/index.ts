import { UsersState } from './usersReducer';
import { FollowersState, ReposState, UserDetailsState } from './userReducer';

export interface RootState {
  users: UsersState,
  user: {
    repos: ReposState,
    followers: FollowersState,
    userDetails: UserDetailsState
  }
}

export { default as usersReducer } from './usersReducer';
export { default as userReducer } from './userReducer';
