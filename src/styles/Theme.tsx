import React from 'react';
import { ThemeProvider, DefaultTheme } from 'styled-components';

const theme: DefaultTheme = {
  colors: {
    dark: '#0a043c',
    dark2: '#1d263a',
    dark3: '#161f33',
    black: '#000000',
    orange: '#ff794d',
    mint: '#d6ebe4',
    lightGrey: '#f9fafb',
    grey: '#ececec',
    grey2: '#d8d8d8',
    white: '#fff',
    text: {
      white: '#fff',
      grey: '#cfcfcf',
      grey2: '#7f828a',
      dark: '#262729',
      dark2: '#1d263a',
      dark3: '#213053',
    },
  },
  fontSizes: {
    xxs: '0.625rem',
    xs: '0.75rem',
    sm: '0.875rem',
    md: '1rem',
    lg: '1.125rem', // 18px
    xl: '1.25rem', // 20px
    xl2: '1.5rem', // 24 
    xl3: '1.75rem', /// 28
    xl4: '1.875rem', // 30
    xl5: '2rem', // 32
    xl6: '2.125rem', // 34
    xl7: '2.25rem' // 36
  },
  fontWeights: {
    hairline: 100,
    thin: 200,
    light: 300,
    normal: 400,
    medium: 500,
    semibold: 600,
    bold: 700,
  },
  borderRadius: {
    sm: '10px',
    md: '15px',
    circle: '50%'
  },
  breakpoints: {
    sm: '576px',
    md: '768px',
    lg: '992px',
    xl: '1200px',
    xl2: '1400px'
  }
};

type ThemeProps = {
  children: JSX.Element
} 

const Theme = ({ children }: ThemeProps): JSX.Element => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);

export default Theme;