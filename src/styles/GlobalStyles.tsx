import styled, { createGlobalStyle } from 'styled-components';

const GlobalStyles = createGlobalStyle`
    *, *::before, *::after {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
    }
    
    ol,
    ul {
    list-style-type: none;
    }
    
    a {
    text-decoration: none;
    }

    body {
      font-family: 'DM Sans', sans-serif;
      line-height: 1.6;
    }
`;

type LoaderContainerProps = {
  verticalPadding?: string;
};

export const LoaderContainer = styled.div<LoaderContainerProps>`
  margin: ${({verticalPadding }) => verticalPadding ? verticalPadding : '2'}rem 0;
  display: flex;
  justify-content: center;
`;

export default GlobalStyles;