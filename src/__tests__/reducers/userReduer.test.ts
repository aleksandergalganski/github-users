import { followersReducer } from '../../redux/reducers/userReducer';
import { 
  GET_FOLLOWERS_FAILURE,
  GET_FOLLOWERS_REQUEST,
  GET_FOLLOWERS_SUCCESS
} from '../../redux/actions/types/userActions';

describe('followersReducer', () => {
  test('should handle GET_FOLLOWERS_REQUEST', () => {
    const expected = {
      data: [],
      isLoading: true,
      error: null
    };

    expect(
      followersReducer(undefined, {
        type: GET_FOLLOWERS_REQUEST
      })
    ).toEqual(expected);
  });

  test('should handle GET_FOLLOWERS_SUCCESS', () => {
    const expected = {
      data: [
        { login: 'test-1', id: 1 },
        { login: 'test-2', id: 2 }
      ],
      isLoading: false,
      error: null,
    };

    expect(
      followersReducer(undefined, {
        type: GET_FOLLOWERS_SUCCESS,
        payload: [
          { login: 'test-1', id: 1 },
          { login: 'test-2', id: 2 }
        ],
      }),
    ).toEqual(expected);
  });

  test('should handle GET_FOLLOWERS_FAILURE', () => {
    const expected = {
      data: [],
      isLoading: false,
      error: 'error',
    };

    expect(
      followersReducer(undefined, {
        type: GET_FOLLOWERS_FAILURE,
        payload: 'error'
      }),
    ).toEqual(expected);
  });
});