import { AnyAction } from 'redux';
import configureStore, { MockStoreEnhanced } from 'redux-mock-store';
import thunk, { ThunkDispatch } from 'redux-thunk';
import MockAdapter from 'axios-mock-adapter';

import githubApi from '../../api/githubApi';
import { getUserRepos } from '../../redux/actions/userActions';
import { RootState } from '../../redux/reducers';

type DispatchExts = ThunkDispatch<RootState, void, AnyAction>;

const middlewares = [thunk];
const mockStore = configureStore<RootState, DispatchExts>(middlewares);
const initialStoreState = {
  users: {
    data: [],
    isLoading: false,
    error: null,
    totalCount: null,
    incompleteResults: null,
  },
  user: {
    userDetails: {
      data: null,
      isLoading: false,
      error: null,
    },
    repos: {
      data: [],
      isLoading: false,
      error: null,
    },
    followers: {
      data: [],
      isLoading: false,
      error: null,
    },
  },
};


describe('user action thunks', () => {
  let mockAdapter: MockAdapter;
  const testUser = 'test-user';
  let store: MockStoreEnhanced<RootState, DispatchExts>;

  beforeEach(() => {
    mockAdapter = new MockAdapter(githubApi);
    store = mockStore(initialStoreState);
  });

  describe('getRepos', () => {
    test('should create GET_REPOS_SUCCES', async () => {
      mockAdapter.onGet(`/users/${testUser}/repos`).reply(200, {});

      const expectedActions = [
        { type: 'GET_REPOS_REQUEST' }, 
        { type: 'GET_REPOS_SUCCESS', payload: {} }
      ];

      await store.dispatch(getUserRepos(testUser));

      const actions = store.getActions();

      expect(actions).toEqual(expectedActions);
    });

    test('should create GET_REPOS_FAILURE', async () => {
      mockAdapter.onGet(`/users/${testUser}/repos`).reply(400);

      const expectedActions = [
        { type: 'GET_REPOS_REQUEST' },
        { type: 'GET_REPOS_FAILURE', payload: 'Get repos error' }
      ];
      await store.dispatch(getUserRepos(testUser));

      const actions = store.getActions();

      expect(actions).toEqual(expectedActions);
    });
  });
});