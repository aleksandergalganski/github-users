import React from 'react';

import { render, screen } from '../../utils/testUtils';
import { Followers } from '../../components/Followers';

describe('Followers component', () => {

  test('should render list of followers', async () => {
    const state = {
      user: {
        followers: {
          data: [
            { id: 1, login: 'test-user1' },
            { id: 2, login: 'test-user2' }
          ],
          isLoading: false,
          error: null
        }
      }
    };

    render(<Followers />, { state });

    expect(await screen.findAllByTestId('user-card')).toHaveLength(2);
  });

  test('should render error message', async () => {
    const state = {
      user: {
        followers: {
          data: [],
          isLoading: false,
          error: 'Error',
        },
      }
    };

    render(<Followers />, { state });

    expect(
      await screen.findByText(/Something wrong happened while fetching repos. Try again later./)
    ).toBeInTheDocument();
  });
});