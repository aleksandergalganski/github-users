import React from 'react';

import { render, screen } from '../../utils/testUtils';
import { Repos } from '../../components/Repos';

describe('Repos component', () => {
  test('should render list of repos', async () => {
    const state = {
      user: {
        repos: {
          data: [
            { id: 1, name: 'repo-1' },
            { id: 2, name: 'repo-2' },
          ],
          isLoading: false,
          error: null,
        },
      },
    };

    render(<Repos />, { state });

    expect(await screen.findAllByTestId('repo')).toHaveLength(2);
  });

  test('should render error message', async () => {
    const state = {
      user: {
        repos: {
          data: [],
          isLoading: false,
          error: 'Error',
        },
      },
    };

    render(<Repos />, { state });

    expect(
      await screen.findByText(
        /Something wrong happened while fetching repos. Try again later./,
      ),
    ).toBeInTheDocument();
  });
});
