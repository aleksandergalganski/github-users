import React, { ComponentType } from 'react';
import { render as rtlRender, RenderResult } from '@testing-library/react';
import { createStore } from 'redux';
import { MemoryRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import { rootReducer } from '../redux/store';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function render(ui: JSX.Element, options?: { state: any }): RenderResult {
  const store = createStore(rootReducer, options?.state);

  function AllProviders({ children }: { children: JSX.Element}): JSX.Element {
    return (
      <MemoryRouter>
        <Provider store={store}>{children}</Provider>
      </MemoryRouter>
    );
  }

  return rtlRender(ui, {
    wrapper: AllProviders as ComponentType,
    ...options,
  });
}

export * from '@testing-library/react';
export { render };