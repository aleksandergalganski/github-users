module.exports = {
  'env': {
    'browser': true,
    'es2021': true,
    'node': true
  },
  'extends': [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:react/recommended',
    'plugin:react-hooks/recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:jsx-a11y/recommended',
  ],
  'parser': '@typescript-eslint/parser',
  'parserOptions': {
    'ecmaFeatures': {
      'jsx': true
    },
    'ecmaVersion': 12,
    'sourceType': 'module'
  },
  'plugins': [
    'react',
    '@typescript-eslint',
    'testing-library',
    'jest-dom'

  ],
  'rules': {
    'react/prop-types': 'off',
    'semi': ['warn', 'always'],
    'quotes': ['warn', 'single'],
    'indent': ['warn', 2],
    'testing-library/await-async-query': 'warn',
    'testing-library/no-await-sync-query': 'warn',
    'testing-library/no-debug': 'warn',
    'jest-dom/prefer-checked': 'warn',
    'jest-dom/prefer-enabled-disabled': 'warn',
    'jest-dom/prefer-required': 'warn',
    'jest-dom/prefer-to-have-attribute': 'warn'
  }
};
