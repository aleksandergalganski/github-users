# GITHUB USERS

### Installation

```bash
npm i
```

### Run dev server

```bash
npm run start
```

### Run build

```bash
npm run build
```

### Run tests

```bash
npm run test
```

### Run linter

```bash
npm run lint
```