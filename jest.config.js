
module.exports = {
  preset: "ts-jest",
  setupFilesAfterEnv: ['<rootDir>/setUpTests.js'],
  testEnvironment: "jsdom",
  roots: ["./src"],
  transform: { "\\.(ts|tsx|js)$": ["ts-jest"] },
  testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$",
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
  moduleNameMapper: {
    "\\.(sass|scss)$": "identity-obj-proxy"
  }
};
