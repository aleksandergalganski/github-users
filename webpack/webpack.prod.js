const path = require('path');
const { merge } = require('webpack-merge');

const commonConfig = require('./webpack.common');

const prodConfig = {
  mode: 'production',
  output: {
    path: path.resolve(__dirname, '..', './build'),
    filename: '[name][contenthash].bundle.js',
    publicPath: '/'
  }
};

module.exports = merge(commonConfig, prodConfig);