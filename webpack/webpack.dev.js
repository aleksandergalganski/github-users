const { merge } = require('webpack-merge');
const ESLintPlugin = require('eslint-webpack-plugin');

const commonConfig = require('./webpack.common');

const devConfig = {
  mode: 'development',
  devServer: {
    port: 3000,
    open: true,
    hot: true,
    historyApiFallback: true,
  },
  plugins: [
    new ESLintPlugin()
  ]
};

module.exports = merge(commonConfig, devConfig);  